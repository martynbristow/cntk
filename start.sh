cd /cntk/Examples/Image/DataSets/Grocery/
python install_grocery.py
cd /cntk/PretrainedModels/
python download_model.py AlexNet_ImageNet_Caffe
cd /cntk/Examples/Image/Detection/FastRCNN
python install_data_and_model.py
python /cntk/Examples/Image/DataSets/Pascal/install_pascalvoc.py
cd /cntk/Examples/Image/DataSets/Pascal/mappings/
python create_mappings.py
cd /cntk/Examples/Image/Detection/
python DetectionDemo.py
