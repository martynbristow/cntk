FROM microsoft/cntk:2.5.1-cpu-python2.7
# Python 3 didn't work, issues installing opencv from python whl and using 

RUN apt-get update
RUN apt-get -y install python2.7 python-pip python-dev libsm6 libxext6 libfontconfig1 libxrender1
RUN wget https://bootstrap.pypa.io/get-pip.py
RUN python get-pip.py
RUN pip install opencv-python
RUN pip install easydict
RUN pip install pyyaml
RUN pip install dlib
